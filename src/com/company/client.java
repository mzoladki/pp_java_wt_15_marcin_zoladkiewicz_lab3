package com.company;
import java.io.*;
import java.net.*;
import java.util.Scanner;

/**
 * Created by marcin on 18.03.17.
 */
public class client {
    public static void main(String[] args)
    {
        int liczbaTalii = 1;

        try{
            Socket s = new Socket("127.0.0.1",5000);
            Deck talia = new Deck(liczbaTalii);
            Player gracz = new Player();
            Croupier krupier = new Croupier();

            ObjectOutputStream Oos = new ObjectOutputStream(s.getOutputStream());
            ObjectInputStream Ois = new ObjectInputStream(s.getInputStream());

            DataInputStream din = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());

            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            Scanner odczyt = new Scanner(System.in);

            String msgin="", msgout="", line="____________________",car;

            System.out.println("hit - pobranie karty");
            System.out.println("double - podwojenie stawki, koniec tury");
            System.out.println("stand - koniec tury");

            krupier.start(talia);
            gracz.start(talia);

            gracz.show2();
            krupier.showOne();

            while(!msgout.equals("stand"))
            {
                if(msgout.equals("hit"))
                {
                    gracz.hit(talia);
                }
                if(msgout.equals("double"))
                {
                    msgout = gracz.doubleTake(talia);
                }
                if(gracz.isBlackJack())
                {
                    System.out.println("GRACZ WYGRAŁ!");
                    System.exit(0);
                }
                if(gracz.isMore())
                {
                    System.out.println("GRACZ PRZEGRAŁ!");
                    System.exit(0);
                }
                for(int i=0;i<gracz.getSet().size();i++)
                {
                    car = gracz.show(i);
                    dout.writeUTF(car);
                }
                dout.writeUTF(line);
                msgout = br.readLine();
                dout.writeUTF(msgout);
            }

            Oos.writeObject(talia);
            Oos.writeObject(gracz);
            Oos.writeObject(krupier);

            while(!msgin.equals("stand")) {
                msgin = din.readUTF();
                System.out.println(msgin);
            }

        }
        catch(Exception e)
        {

        }
    }
}
