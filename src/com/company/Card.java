package com.company;
import java.io.Serializable;

public class Card implements Serializable {
	
	private int wage; 
	private char value;
	private char colour;
	
	public Card(char v, char c)
	{
		value=v;
		colour=c;
		if(v=='Q' || v=='K' || v=='J' || v=='T')
		{
			wage=10;
		}
		else if(v == 'A')
		{
			wage=11;
		}
		else
		{
			wage=Character.getNumericValue(v);
		}
	}
	public int getWage(){ return wage;}
	public void show()
	{
		System.out.println(value+" "+colour);
	}

	public char getValue() {
		return value;
	}

	public char getColour() {
		return colour;
	}
}
