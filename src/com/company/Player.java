package com.company;

import java.io.Serializable;
import java.util.Vector;

public class Player implements Serializable {



	private Vector<Card> set = new Vector<Card>();
	private int wageOfSet = 0;

	public Vector<Card> getSet() {
		return set;
	}
	public boolean isBlackJack()
	{
		if (getWageOfSet() == 21 || getWageOfSet2() == 21) return true;
		else return false;
	}
	public boolean isMore()
	{
		if (getWageOfSet() > 21 && getWageOfSet2() > 21) return true;
		else return false;
	}
	public void hit(Deck x)
	{
		add(x.take());
		show2();
	}

	public String doubleTake(Deck x)
	{
		add(x.take());
		show2();
		return "stand";
	}

	public int getWageOfSet2() {
		return wageOfSet2;
	}

	private int wageOfSet2= 0;

	public void add(Card x)
	{
		set.addElement(x);
		wageOfSet += x.getWage();
		if(x.getValue()=='A')
		{
			wageOfSet2+=1;
		}
		else wageOfSet2+=x.getWage();

	}

	public void start(Deck x)
	{
		add(x.take());
		add(x.take());
	}
	
	public String show(int i)
	{
		String karta = set.get(i).getValue() + " " + set.get(i).getColour();
		return karta;

	}
	public void show2() {
		System.out.println("Karty gracza: ");
		for (int i = 0; i < set.size(); i++)
			set.get(i).show();
		if (wageOfSet2 == wageOfSet)
			System.out.println(String.format("Wartość zestawu: %d", getWageOfSet()));
		else System.out.println(String.format("Wartosc zestawu: %d/%d", getWageOfSet(), getWageOfSet2()));
	}

	public int getWageOfSet(){ return wageOfSet;}
		
}
