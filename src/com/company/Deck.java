package com.company;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;


public class Deck implements Serializable {
	
	ArrayList<Card> set = new ArrayList<Card>();
	
	public Deck(int liczbaTalii)
	{
		char[] colourTable={'C','S','D','H'};
		for(int j=0;j<liczbaTalii;j++)
		{
			for(int i=0;i<4;i++)
			{
				set.add(new Card('2',colourTable[i]));
				set.add(new Card('3',colourTable[i]));
				set.add(new Card('4',colourTable[i]));
				set.add(new Card('5',colourTable[i]));
				set.add(new Card('6',colourTable[i]));
				set.add(new Card('7',colourTable[i]));
				set.add(new Card('8',colourTable[i]));
				set.add(new Card('9',colourTable[i]));
				set.add(new Card('T',colourTable[i]));
				set.add(new Card('J',colourTable[i]));
				set.add(new Card('Q',colourTable[i]));
				set.add(new Card('K',colourTable[i]));
				set.add(new Card('A',colourTable[i]));
			}
		}
	}

	public Card take()
	{
		Random r = new Random();
		return set.remove(r.nextInt(set.size()));
	}

	public void show()
	{
		for(Card temp: set) temp.show();
	}
}
