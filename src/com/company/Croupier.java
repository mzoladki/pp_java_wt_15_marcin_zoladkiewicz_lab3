package com.company;

import java.io.Serializable;
import java.util.Vector;

public class Croupier implements Serializable {

    private Vector<Card> set = new Vector<Card>();
    private int wageOfSet =0;
    private int wageOfSet2=0;

    public boolean isBlackJack()
    {
        if (getWageOfSet() == 21 || getWageOfSet2() == 21) return true;
        else return false;
    }
    public boolean isMore()
    {
        if (getWageOfSet() > 21 && getWageOfSet2() > 21) return true;
        else return false;
    }
    public void hit(Deck x) {
        add(x.take());
        show();
    }

    public int getWageOfSet(){
        return wageOfSet;
    }

    public int getWageOfSet2() {
        return wageOfSet2;
    }

    public void add(Card x)
    {
        set.addElement(x);
        wageOfSet += x.getWage();
        if(x.getValue()=='A')
        {
            wageOfSet2+=1;
        }
        else wageOfSet2=wageOfSet;

    }

    public void start(Deck x)
    {
        add(x.take());
        add(x.take());
    }
    public void showOne()
    {
        System.out.println("Karta krupiera:");
        set.get(0).show();
        System.out.println(set.get(0).getWage());
    }

    public char getFirstCard()
    {
        return set.get(0).getValue();
    }

    public void show()
    {
        System.out.println("Karty krupiera: ");
        for(int i=0;i<set.size();i++)
            set.get(i).show();
        if(wageOfSet2==wageOfSet)
            System.out.println(String.format("Wartość zestawu: %d", getWageOfSet()));
        else System.out.println(String.format("Wartosc zestawu: %d/%d",getWageOfSet(), getWageOfSet2()));
    }
}
